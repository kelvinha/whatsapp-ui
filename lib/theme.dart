import 'package:flutter/material.dart';

Color bgColor = Color(0xFF085e55);
Color primaryColor = Color(0xFF1fc053);

TextStyle titleTextStyle = TextStyle(
  color: Colors.black,
  fontSize: 16,
  fontWeight: FontWeight.bold,
);

TextStyle subTitleTextStyle = TextStyle(
  color: Colors.black,
  fontSize: 14,
  fontWeight: FontWeight.w300,
);
