import 'package:flutter/material.dart';
import 'package:flutter_application_1/pages/ChatPage.dart';
import 'package:flutter_application_1/theme.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () {},
          backgroundColor: primaryColor,
          child: Icon(Icons.message_sharp),
        ),
        appBar: AppBar(
          actions: [
            Icon(Icons.search),
            SizedBox(
              width: 10,
            ),
            Icon(Icons.more_vert),
            SizedBox(
              width: 10,
            ),
          ],
          backgroundColor: bgColor,
          title: Text('WhatsApp'),
          bottom: TabBar(
            indicatorColor: Colors.white,
            indicatorWeight: 3.0,
            tabs: [
              Tab(
                child: Text('CHATS'),
              ),
              Tab(
                child: Text('STATUS'),
              ),
              Tab(
                child: Text('CALLS'),
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            Tab(
              child: ListView(
                children: [
                  Column(
                    children: [
                      ListChat(
                        gambar: 'assets/logo.png',
                        judul: 'LinkedIn',
                        isiChat: 'Halo, Saya admin linkedin ingin menawarkan..',
                        waktu: '17:55',
                        isAsset: true,
                      ),
                      ListChat(
                        gambar: 'assets/jang.jpg',
                        judul: 'Wonyoung',
                        isiChat: 'Aku mau balikan..',
                        waktu: '20:55',
                        isAsset: true,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Tab(
              child: Text('Ini view Status'),
            ),
            Tab(
              child: Text('Ini view Calls'),
            ),
          ],
        ),
      ),
    );
  }
}

class ListChat extends StatelessWidget {
  final String gambar;
  final String judul;
  final String isiChat;
  final String waktu;
  final bool isAsset;

  ListChat({this.gambar, this.judul, this.isiChat, this.waktu, this.isAsset});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ChatPage(
              nama: judul,
              profile: gambar,
              isAsset: isAsset,
              isiChat: isiChat,
            ),
          ),
        );
      },
      contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
      leading: CircleAvatar(
        radius: 25,
        backgroundImage: isAsset ? AssetImage(gambar) : NetworkImage(gambar),
      ),
      title: Row(
        children: [
          Text(
            judul,
            style: titleTextStyle,
          ),
          Icon(
            Icons.verified,
            size: 16,
            color: primaryColor,
          ),
        ],
      ),
      subtitle: Text(
        isiChat,
        style: subTitleTextStyle,
      ),
      trailing: Text(waktu),
    );
  }
}
