import 'package:flutter/material.dart';
import 'package:flutter_application_1/theme.dart';

class ChatPage extends StatefulWidget {
  final String nama;
  final String profile;
  final bool isAsset;
  final String isiChat;
  ChatPage({this.nama, this.profile, this.isAsset, this.isiChat});

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  double total;
  @override
  void initState() {
    total = widget.isiChat.length + .0;
    print(total);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width / 1.5 + total,
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(10.0),
                  bottomLeft: Radius.circular(10.0),
                  topRight: Radius.circular(10.0),
                ),
                color: Color(0xffFFFFFF),
              ),
              child: Row(
                children: [
                  Text(widget.isiChat),
                ],
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
          ],
        ),
      ),
      backgroundColor: Color(0xFFeae1d9),
      appBar: AppBar(
        backgroundColor: bgColor,
        leadingWidth: 28.0,
        actions: [
          Icon(Icons.videocam_rounded),
          SizedBox(
            width: 15.0,
          ),
          Icon(Icons.phone),
          SizedBox(
            width: 15.0,
          ),
          Icon(Icons.more_vert),
          SizedBox(
            width: 15.0,
          ),
        ],
        title: Row(
          children: [
            CircleAvatar(
              radius: 20,
              backgroundImage: widget.isAsset
                  ? AssetImage(widget.profile)
                  : NetworkImage(widget.profile),
            ),
            SizedBox(
              width: 10.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.nama,
                  style: titleTextStyle.copyWith(color: Colors.white),
                ),
                Text(
                  'Last seen today 19:20 PM',
                  style: subTitleTextStyle.copyWith(color: Colors.white),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
